# CI 環境では o+w になってて、ansible.cfg を読み込んでくれないので
chmod o-rwx .

# setup virtualenv
virtualenv venv
source venv/bin/activate

# setup ansible
pip install -r requirements.txt
ansible-galaxy install -r requirements.yml
