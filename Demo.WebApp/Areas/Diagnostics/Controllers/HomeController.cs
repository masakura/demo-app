using Demo.WebApp.Areas.Diagnostics.Helpers;
using Demo.WebApp.Areas.Diagnostics.Models.Home;
using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApp.Areas.Diagnostics.Controllers;

public sealed class HomeController : DiagnosticsController
{
    private readonly EnvironmentViewModel _environment;

    public HomeController(EnvironmentViewModel environment)
    {
        _environment = environment;
    }

    public IActionResult Index()
    {
        return View(_environment);
    }
}