using System.Data;
using Demo.WebApp.Areas.Diagnostics.Helpers;
using Demo.WebApp.Areas.Diagnostics.Models.Database;
using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApp.Areas.Diagnostics.Controllers;

public sealed class DatabaseController : DiagnosticsController
{
    private readonly IDbConnection _connection;

    public DatabaseController(IDbConnection connection)
    {
        _connection = connection;
    }

    public IActionResult Index()
    {
        return View(DbConnectionState.From(_connection));
    }
}