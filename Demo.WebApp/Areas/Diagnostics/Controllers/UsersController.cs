using System.Collections.Immutable;
using Demo.WebApp.Areas.Diagnostics.Helpers;
using Demo.WebApp.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApp.Areas.Diagnostics.Controllers;

public sealed class UsersController : DiagnosticsController
{
    private readonly UserManager<ApplicationUser> _userManager;

    public UsersController(UserManager<ApplicationUser> userManager)
    {
        _userManager = userManager;
    }

    public IActionResult Index()
    {
        var users = _userManager.Users.ToImmutableArray();
        return View(users);
    }
}