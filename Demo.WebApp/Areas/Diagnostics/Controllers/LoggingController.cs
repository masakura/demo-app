using Demo.WebApp.Areas.Diagnostics.Helpers;
using Microsoft.AspNetCore.Mvc;
using Sentry;

namespace Demo.WebApp.Areas.Diagnostics.Controllers;

public sealed class LoggingController : DiagnosticsController
{
    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Sentry(string text)
    {
        SentrySdk.CaptureMessage(text);

        return RedirectToAction(nameof(Index));
    }
}