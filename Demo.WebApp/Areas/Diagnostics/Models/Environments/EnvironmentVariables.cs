using System.Collections;

namespace Demo.WebApp.Areas.Diagnostics.Models.Environments;

public sealed class EnvironmentVariables : IEnumerable<EnvironmentVariable>
{
    public IEnumerator<EnvironmentVariable> GetEnumerator()
    {
        var variables = Environment.GetEnvironmentVariables();
        return variables.Keys.OfType<string>()
            .Select(key => new EnvironmentVariable(key, (string?) variables[key]))
            .GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}