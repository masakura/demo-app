namespace Demo.WebApp.Areas.Diagnostics.Models.Environments;

public sealed record EnvironmentVariable(string Key, string? Value);