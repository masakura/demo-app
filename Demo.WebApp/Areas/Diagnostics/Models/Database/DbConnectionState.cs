using System.Data;
using Dapper;

namespace Demo.WebApp.Areas.Diagnostics.Models.Database;

public sealed record DbConnectionState(string ConnectionString, Exception? Exception)
{
    public bool IsConnected => Exception == null;

    public static DbConnectionState From(IDbConnection connection)
    {
        try
        {
            connection.Open();
            connection.Execute("select version();");
            return new DbConnectionState(connection.ConnectionString, null);
        }
        catch (Exception ex)
        {
            return new DbConnectionState(connection.ConnectionString, ex);
        }
        finally
        {
            connection.Close();
        }
    }
}