using System.Collections.Immutable;
using Sentry;

namespace Demo.WebApp.Areas.Diagnostics.Models.Navigation;

internal sealed record MenuItem(string Label, string ControllerName, string ActionName = "Index")
{
    public static MenuItem 環境 { get; } = new(nameof(環境), "Home");
    public static MenuItem データベース { get; } = new(nameof(データベース), "Database");
    public static MenuItem ユーザー { get; } = new(nameof(ユーザー), "Users");
    public static MenuItem ログ { get; } = new(nameof(ログ), "Logging");

    public static ImmutableArray<MenuItem> All { get; } = Create().ToImmutableArray();

    private static IEnumerable<MenuItem> Create()
    {
        yield return 環境;
        yield return データベース;
        yield return ユーザー;

        if (SentrySdk.IsEnabled) yield return ログ;
    }
}