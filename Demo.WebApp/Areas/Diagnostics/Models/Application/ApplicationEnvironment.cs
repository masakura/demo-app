namespace Demo.WebApp.Areas.Diagnostics.Models.Application;

public sealed class ApplicationEnvironment
{
    private readonly IWebHostEnvironment _environment;

    public ApplicationEnvironment(IWebHostEnvironment environment)
    {
        _environment = environment;
    }

    public string ApplicationName => _environment.ApplicationName;
    public string EnvironmentName => _environment.EnvironmentName;
}