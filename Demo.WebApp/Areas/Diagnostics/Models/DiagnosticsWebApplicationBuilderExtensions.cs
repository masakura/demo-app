using Demo.WebApp.Areas.Diagnostics.Models;
using Demo.WebApp.Areas.Diagnostics.Models.Application;
using Demo.WebApp.Areas.Diagnostics.Models.Environments;
using Demo.WebApp.Areas.Diagnostics.Models.Home;

// ReSharper disable once CheckNamespace
namespace Microsoft.AspNetCore.Builder;

internal static class DiagnosticsWebApplicationBuilderExtensions
{
    public static void AddDiagnosticsServices(this WebApplicationBuilder app)
    {
        var configuration = app.Configuration.GetSection("DiagnosticsPanel");
        var diagnostics = DiagnosticsOptions.From(configuration);
        app.Services.Configure<DiagnosticsOptions>(configuration);

        if (diagnostics.Enabled) app.Services.AddDiagnosticsServices();
    }

    private static void AddDiagnosticsServices(this IServiceCollection services)
    {
        services
            .AddScoped<ApplicationEnvironment>()
            .AddScoped<EnvironmentVariables>()
            .AddScoped<EnvironmentViewModel>();
    }
}