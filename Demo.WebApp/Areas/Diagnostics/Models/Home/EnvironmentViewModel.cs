using Demo.WebApp.Areas.Diagnostics.Models.Application;
using Demo.WebApp.Areas.Diagnostics.Models.Environments;

namespace Demo.WebApp.Areas.Diagnostics.Models.Home;

public sealed class EnvironmentViewModel
{
    public EnvironmentViewModel(ApplicationEnvironment application, EnvironmentVariables environmentVariables)
    {
        EnvironmentVariables = environmentVariables;
        Application = application;
    }

    public ApplicationEnvironment Application { get; }
    public EnvironmentVariables EnvironmentVariables { get; }
}