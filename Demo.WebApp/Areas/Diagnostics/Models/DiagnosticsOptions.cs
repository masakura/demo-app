namespace Demo.WebApp.Areas.Diagnostics.Models;

internal sealed record DiagnosticsOptions
{
    public bool Enabled { get; init; }

    public static DiagnosticsOptions Empty { get; } = new() {Enabled = false};

    public static DiagnosticsOptions From(IConfiguration configuration)
    {
        return configuration.Get<DiagnosticsOptions>() ?? Empty;
    }
}