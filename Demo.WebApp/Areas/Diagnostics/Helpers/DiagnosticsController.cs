using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApp.Areas.Diagnostics.Helpers;

[DiagnosticsArea]
public abstract class DiagnosticsController : Controller
{
}