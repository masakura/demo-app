using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApp.Areas.Diagnostics.Helpers;

public sealed class DiagnosticsAreaAttribute : AreaAttribute
{
    public DiagnosticsAreaAttribute() : base("Diagnostics")
    {
    }
}