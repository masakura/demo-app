using System.Data;
using Npgsql;

namespace Demo.WebApp.Data;

internal static class DataExtensions
{
    public static void AddDatabase(this WebApplicationBuilder app)
    {
        var connectionString = ConnectionString(app.Configuration);

        app.Services.AddScoped<IDbConnection>(_ => new NpgsqlConnection(connectionString));
    }

    private static string ConnectionString(this IConfiguration configuration)
    {
        return configuration.GetConnectionString("Default");
    }
}