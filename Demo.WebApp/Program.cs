using Demo.WebApp.Data;
using Demo.WebApp.Identity;
using Demo.WebApp.Sentry;
using GitLab.FeatureFlags;

var builder = WebApplication.CreateBuilder(args)
    .ConfigureSentry();

builder.AddDiagnosticsServices();

builder.AddDatabase();

builder.Services
    .AddApplicationIdentity()
    .AddFeatureFlags();

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.UseGitLabVisualReviews();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
        "Areas",
        "{area:exists}/{controller=Home}/{action=Index}/{id?}");
    endpoints.MapControllerRoute(
        "default",
        "{controller=Home}/{action=Index}/{id?}");
});

app.Run();