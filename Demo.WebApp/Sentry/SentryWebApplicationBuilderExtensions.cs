namespace Demo.WebApp.Sentry;

internal static class SentryWebApplicationBuilderExtensions
{
    public static WebApplicationBuilder ConfigureSentry(this WebApplicationBuilder app)
    {
        app.WebHost.UseSentry(o =>
        {
            o.DefaultTags.Add("gitlab_environment", Environment.GetEnvironmentVariable("GITLAB_ENVIRONMENT")!);
        });
        return app;
    }
}