namespace Demo.WebApp.Identity;

public sealed class ApplicationUser
{
#pragma warning disable CS8618
    // ReSharper disable once UnusedMember.Local
    private ApplicationUser()
#pragma warning restore CS8618
    {
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public ApplicationUser(Guid id, string userName)
    {
        Id = id;
        UserName = userName;
    }

    public Guid Id { get; }
    public string UserName { get; }

    public static ApplicationUser New(string userName)
    {
        return new ApplicationUser(Guid.NewGuid(), userName);
    }
}