using System.Data;
using Dapper;
using Microsoft.AspNetCore.Identity;

namespace Demo.WebApp.Identity;

// ReSharper disable once ClassNeverInstantiated.Global
internal sealed class ApplicationUserStore : IUserPasswordStore<ApplicationUser>, IQueryableUserStore<ApplicationUser>
{
    private readonly IDbConnection _connection;

    public ApplicationUserStore(IDbConnection connection)
    {
        _connection = connection;
    }

    public IQueryable<ApplicationUser> Users
    {
        get
        {
            const string sql = @"
select id, user_name as UserName
from users;
";

            return _connection.Query<ApplicationUser>(sql).AsQueryable();
        }
    }

    public async Task SetPasswordHashAsync(ApplicationUser user, string passwordHash,
        CancellationToken cancellationToken)
    {
        const string sql = @"
delete from passwords where user_id = @Id;
insert into passwords(user_id, password) values(@Id, @Password);
";

        await _connection.ExecuteAsync(sql, new {user.Id, Password = passwordHash});
    }

    public Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        const string sql = @"
select password from passwords
where user_id = @Id; 
";

        return _connection.QueryFirstOrDefaultAsync<string>(sql, new {user.Id});
    }

    public Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        throw new NotSupportedException();
    }

    public void Dispose()
    {
        _connection.Dispose();
    }

    public Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        return Task.FromResult(user.Id.ToString());
    }

    public Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        return Task.FromResult(user.UserName);
    }

    public Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
    {
        throw new NotSupportedException();
    }

    public Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        throw new NotSupportedException();
    }

    public Task SetNormalizedUserNameAsync(ApplicationUser user,
        string normalizedName,
        CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }

    public async Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        const string sql = @"
insert into users(id,user_name)
values(@Id, @UserName);
";

        var affects = await _connection.ExecuteAsync(sql, user);
        if (affects > 0) return IdentityResult.Success;
        return IdentityResult.Failed(new IdentityError());
    }

    public async Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        const string sql = @"
update users set user_name = @UserName
where id = @Id;
";

        var affects = await _connection.ExecuteAsync(sql, user);

        if (affects > 0) return IdentityResult.Success;
        return IdentityResult.Failed(new IdentityError());
    }

    public async Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
    {
        const string sql = @"
delete from passwords where user_id = @Id;
delete from users where id = @Id;
";

        var affects = await _connection.ExecuteAsync(sql, new {user.Id});

        if (affects > 0) return IdentityResult.Success;
        return IdentityResult.Failed(new IdentityError());
    }

    public async Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
    {
        const string sql = @"
select id, user_name as UserName
from users
where id = @Id;
";

        return await _connection.QueryFirstOrDefaultAsync<ApplicationUser>(sql, new {Id = Guid.Parse(userId)});
    }

    public async Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
    {
        const string sql = @"
select id, user_name as UserName
from users
where user_name = @UserName;
";

        return await _connection.QueryFirstOrDefaultAsync<ApplicationUser>(sql, new {UserName = normalizedUserName});
    }
}