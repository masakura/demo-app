using System.Collections.Generic;
using System.IO;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;
using Nuke.Common.IO;

namespace Compress;

static class ZipArchiver
{
    public static void Create(AbsolutePath zipFile, AbsolutePath rootDirectory)
    {
        Directory.CreateDirectory(zipFile.Parent!);
        
        using var zip = ZipFile.Create(zipFile);
        zip.BeginUpdate();

        var currentDirectory = Directory.GetCurrentDirectory();
        Directory.SetCurrentDirectory(rootDirectory);

        try
        {
            zip.AddFiles(GetFiles(rootDirectory));
            zip.AddDirectories(GetDirectories(rootDirectory));

            zip.CommitUpdate();
        }
        finally
        {
            Directory.SetCurrentDirectory(currentDirectory);
        }
    }

    static IEnumerable<RelativePath> GetFiles(AbsolutePath path) =>
        path.GlobFiles("**/*")
            .Select(item => path.GetRelativePathTo(item));

    static IEnumerable<RelativePath> GetDirectories(AbsolutePath path) =>
        path.GlobDirectories("**/*")
            .Select(item => path.GetRelativePathTo(item));

    static void AddFiles(this ZipFile zip, IEnumerable<RelativePath> files)
    {
        foreach (var path in files) zip.Add(path);
    }

    static void AddDirectories(this ZipFile zip, IEnumerable<RelativePath> directories)
    {
        foreach (var path in directories) zip.AddDirectory(path);
    }
}