using Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;
using Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Nuke;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.ReSharper;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

namespace Builds;

public interface ILint : ISolution
{
    AbsolutePath LintReportDirectory => ReportDirectory / "lint";

    private AbsolutePath InspectCodeFile => LintReportDirectory / "inspect-code.xml";
    private AbsolutePath CodeQualityFile => LintReportDirectory / "code-quality.json";
    private AbsolutePath InspectCodeCacheDirectory => CacheDirectory / "inspect-code";

    // ReSharper disable once UnusedMember.Global
    Target Lint => _ => _
        .DependsOn<ICompile>(x => x.Compile)
        .Executes(() =>
        {
            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .AddProperty("Configuration", Configuration)
                .SetProcessArgumentConfigurator(args => args.Add("--no-build"))
                .SetOutput(InspectCodeFile)
                .SetCachesHome(InspectCodeCacheDirectory));
        });

    // ReSharper disable once UnusedMember.Global
    Target CodeQuality => _ => _
        .TriggeredBy(Lint)
        .Executes(() =>
        {
            ReSharperReporterTasks.ReSharperReport(s => s
                .SetInput(InspectCodeFile)
                .SetSeverity(EnsureSeverityLevel.All)
                .AddExport<CodeQualityConverter>(CodeQualityFile)
                .AddExport<SummaryConverter>());
        });
}