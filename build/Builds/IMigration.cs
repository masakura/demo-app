using System.Linq;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using Ignis.Nuke.FluentMigrator;
using Microsoft.Extensions.DependencyInjection;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.DotNet;
using static Ignis.Nuke.FluentMigrator.FluentMigratorTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

interface IMigration : ICompile, IDatabase
{
    private AbsolutePath MigrationsDirectory => OutputDirectory / "migrations";

    Target PublishMigrations => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetPublish(s => s
                .SetProject(Solution.GetProject("Demo.Database.Migrations"))
                .SetConfiguration(Configuration)
                .SetOutput(MigrationsDirectory)
                .SetAssemblyVersion(GitVersion.AssemblySemVer)
                .SetFileVersion(GitVersion.AssemblySemFileVer)
                .SetInformationalVersion(GitVersion.InformationalVersion)
                .EnableNoBuild()
                .EnableNoRestore());
        });

    // ReSharper disable once UnusedMember.Global
    Target MigrateUp => _ => _
        .DependsOn(PublishMigrations)
        .Executes(() =>
        {
            var migrations = MigrationsDirectory.GlobFiles("*.Migrations.dll").First();

            FluentMigratorMigrateUp(s => s
                .SetConnectionString(Connection)
                .AddAssemblies(migrations)
                .AddConfigureRunner(runner =>
                {
                    runner.Services.Configure<RunnerOptions>(options =>
                    {
                        options.Tags = new[] {"test"};
                    });
                    runner.AddPostgres();
                }));
        });
}