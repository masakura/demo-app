using System.IO;
using System.Linq;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.ReportGenerator;
using Serilog;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.ReportGenerator.ReportGeneratorTasks;

namespace Builds;

interface ITest : ISolution, IDatabase
{
    AbsolutePath TestReportDirectory => ReportDirectory / "junit";
    private AbsolutePath HtmlCodeCoverageReportDirectory => ReportDirectory / "code-coverage";

    // ReSharper disable once UnusedMember.Global
    Target Test => _ => _
        .DependsOn<ICompile>(x => x.Compile)
        .Executes(() =>
        {
            var reports = TestReportDirectory / "{assembly}-test-result.xml";

            DotNetTest(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .SetLoggers(
                    $@"""junit;LogFilePath={reports};MethodFormat=Class;FailureBodyFormat=Verbose""")
                .AddProcessEnvironmentVariable("CUSTOMCONNSTR_Default", Connection)
                .SetProcessArgumentConfigurator(args => args
                    .Add(@"--collect:""XPlat Code Coverage"""))
                .EnableNoBuild()
                .EnableNoRestore());
        });

    // ReSharper disable once UnusedMember.Global
    Target CodeCoverageReport => _ => _
        .TriggeredBy(Test)
        .AssuredAfterFailure()
        .Executes(() =>
        {
            var sources = RootDirectory.GlobFiles("**/coverage.cobertura.xml")
                .Select(path => path.ToString());

            var outputs = ReportGenerator(s => s
                .SetFramework("net6.0")
                .AddReports(sources)
                .SetTargetDirectory(HtmlCodeCoverageReportDirectory)
                .AddReportTypes(ReportTypes.Html)
                .AddReportTypes(ReportTypes.TextSummary));

            var summary = File.ReadAllText(HtmlCodeCoverageReportDirectory / "Summary.txt");
            // ReSharper disable once TemplateIsNotCompileTimeConstantProblem
            Log.Information(summary);
        });
}