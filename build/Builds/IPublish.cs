using Compress;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

public interface IPublish : ISolution
{
    AbsolutePath PublishDirectory => RootDirectory / "publish";
    protected string PackageVersion => GitVersion.NuGetVersionV2;
    protected AbsolutePath PackageFile => OutputDirectory / $"demo-app.{PackageVersion}.zip";

    // ReSharper disable once UnusedMember.Global
    Target Publish => _ => _
        .DependsOn<ICompile>(x => x.Compile)
        .Executes(() =>
        {
            DotNetPublish(s => s
                .SetOutput(PublishDirectory)
                .SetProject(Solution.GetProject("Demo.WebApp"))
                .SetConfiguration(Configuration)
                .SetAssemblyVersion(GitVersion.AssemblySemVer)
                .SetFileVersion(GitVersion.AssemblySemFileVer)
                .SetInformationalVersion(GitVersion.InformationalVersion)
                .EnableNoBuild()
                .EnableNoRestore());

            ZipArchiver.Create(PackageFile, PublishDirectory);
        });
}