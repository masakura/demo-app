using Nuke.Common;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tools.GitVersion;

namespace Builds;

public interface ISolution : INukeBuild
{
    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    Configuration Configuration => IsLocalBuild ? Configuration.Debug : Configuration.Release;

    AbsolutePath OutputDirectory => RootDirectory / "output";
    AbsolutePath ReportDirectory => RootDirectory / "reports";
    AbsolutePath CacheDirectory => RootDirectory / ".cache";
    [Solution] Solution Solution => TryGetValue(() => Solution);
    [GitVersion] GitVersion GitVersion => TryGetValue(() => GitVersion);
    [GitRepository] GitRepository GitRepository => TryGetValue(() => GitRepository);
}