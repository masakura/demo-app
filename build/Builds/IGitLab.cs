using Ignis.GitLab.Api.Client.Authentication;
using Nuke.Common;

namespace Builds;

interface IGitLab : INukeBuild
{
    [Parameter] PrivateTokenCredential? GitLabToken => TryGetValue(() => GitLabToken);

    Credential GitLabCredential => GitLabToken ?? Credential.Default();
    string GitLabProject => "masakura/demo-app";
}