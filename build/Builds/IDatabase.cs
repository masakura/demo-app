using Nuke.Common;

namespace Builds;

interface IDatabase : INukeBuild
{
    [Parameter("データベース接続文字列。")]
    string Connection => TryGetValue(() => Connection) ??
                               "Server=localhost;Port=5432;User Id=demo;Password=P@ssw0rd;";
}