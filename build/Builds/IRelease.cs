using Ignis.Nuke.GitLab;
using Ignis.Nuke.GitLab.Common;
using Ignis.Nuke.GitLab.Projects;
using Ignis.Nuke.GitLab.Releases;
using Nuke.Common;
using Nuke.Common.CI.GitLab;

namespace Builds;

interface IRelease : IPush
{
    [Parameter("Release Target 用の Git Repository Tag の名前。")]
    private string GitTag => TryGetValue(() => GitTag) ?? GitLab.Instance.CommitRefName;

    // ReSharper disable once UnusedMember.Global
    Target Release => _ => _
        .Description("GitLab Release を作成します。")
        .DependsOn(Push)
        .Executes(async () =>
        {
            await GitLabTasks.GitLabCreateReleaseAsync(s => s
                .SetProjectId(GitLabProject)
                .SetCredential(GitLabCredential)
                .SetTagName(GitTag));
        });
}