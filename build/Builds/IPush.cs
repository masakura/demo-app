using System.IO;
using Ignis.Nuke.GitLab.Common;
using Ignis.Nuke.GitLab.Packages;
using Ignis.Nuke.GitLab.Projects;
using Nuke.Common;
using static Ignis.Nuke.GitLab.GitLabTasks;

namespace Builds;

interface IPush : IPublish, IGitLab
{
    Target Push => _ => _
        .Description("GitLab Package Registry にパッケージをアップロードします。")
        .DependsOn(Publish)
        .Executes(async () =>
        {
            await GitLabPublishGenericPackageAsync(s => s
                .SetGitLabUri("https://gitlab.com")
                .SetProjectId(GitLabProject)
                .SetCredential(GitLabCredential)
                .SetFileName(PackageFile.Name)
                .SetPackageName("demo-app")
                .SetContent(File.ReadAllBytes(PackageFile))
                .SetPackageVersion(PackageVersion));
        });
}