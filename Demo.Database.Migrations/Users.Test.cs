using Demo.Database.Migrations.Helpers;
using FluentMigrator;
using FluentMigrator.Builders.Insert;

namespace Demo.Database.Migrations;

[TestData]
[Migration(202202040915)]
// ReSharper disable once UnusedType.Global
public sealed class TestUsers : Migration
{
    public override void Up()
    {
        var users = new[]
        {
            new User("user1", "P@ssw0rd"),
            new User("user2", "P@ssw0rd")
        };

        Insert.IntoTable(Users.TableName)
            .Rows(users.ToUserRows());

        Insert.IntoTable(Passwords.TableName)
            .Rows(users.ToPasswordRows());
    }

    public override void Down()
    {
        throw new NotSupportedException();
    }
}