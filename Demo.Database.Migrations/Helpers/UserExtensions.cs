namespace Demo.Database.Migrations.Helpers;

internal static class UserExtensions
{
    public static IEnumerable<object> ToUserRows(this IEnumerable<User> users)
    {
        return users.Select(user => user.ToUserRow());
    }

    public static IEnumerable<object> ToPasswordRows(this IEnumerable<User> users)
    {
        return users.Select(user => user.ToPasswordRow());
    }
}