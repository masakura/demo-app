using Microsoft.AspNetCore.Identity;

namespace Demo.Database.Migrations.Helpers;

internal sealed record User(string UserName, string Password)
{
    private static readonly PasswordHasher<User> PasswordHasher = new();

    // ReSharper disable once MemberCanBePrivate.Global
    public Guid Id { get; } = Guid.NewGuid();
    public string UserName { get; } = UserName;
    public string Password { get; } = Password;

    public object ToUserRow()
    {
        return new {id = Id, user_name = UserName};
    }

    public object ToPasswordRow()
    {
        return new {user_id = Id, password = PasswordHasher.HashPassword(this, Password)};
    }
}