// ReSharper disable once CheckNamespace
namespace FluentMigrator.Builders.Insert;

internal static class FluentMigratorExtensions
{
    public static void Rows(this IInsertDataOrInSchemaSyntax insert, IEnumerable<object> rows)
    {
        foreach (var row in rows) insert.Row(row);
    }
}