using FluentMigrator;

namespace Demo.Database.Migrations.Helpers;

internal sealed class TestDataAttribute : TagsAttribute
{
    public TestDataAttribute() : base("test")
    {
    }
}