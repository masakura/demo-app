using FluentMigrator;

namespace Demo.Database.Migrations;

[Migration(202202011433)]
// ReSharper disable once UnusedType.Global
// ReSharper disable once ClassNeverInstantiated.Global
public sealed class Passwords : Migration
{
    public static string TableName => nameof(Passwords).ToLowerInvariant();

    public override void Up()
    {
        Create.Table(TableName)
            .WithColumn("user_id").AsGuid().PrimaryKey()
            .WithColumn("password").AsString().NotNullable();
    }

    public override void Down()
    {
        throw new NotSupportedException();
    }
}