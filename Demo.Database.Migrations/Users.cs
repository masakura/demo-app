using FluentMigrator;

namespace Demo.Database.Migrations;

[Migration(202202011429)]
// ReSharper disable once UnusedType.Global
// ReSharper disable once ClassNeverInstantiated.Global
public sealed class Users : Migration
{
    public static string TableName => nameof(Users).ToLowerInvariant();

    public override void Up()
    {
        Create.Table(TableName)
            .WithColumn("id").AsGuid().PrimaryKey()
            .WithColumn("user_name").AsString().NotNullable();
    }

    public override void Down()
    {
        throw new NotSupportedException();
    }
}