using GitLab.VisualReviews;

// ReSharper disable once CheckNamespace
namespace Microsoft.AspNetCore.Builder;

public static class VisualReviewsApplicationBuilderExtensions
{
    public static void UseGitLabVisualReviews(this IApplicationBuilder app)
    {
        if (VisualReviewEnvironment.RequireVisualReviews)
        {
            app.UseMiddleware<VisualReviewsMiddleware>();
        }
    }
}