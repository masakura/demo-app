using System.Web;

namespace GitLab.VisualReviews;

internal sealed class VisualReviewsScript
{
    private readonly VisualReviewEnvironment _environment;

    public VisualReviewsScript(VisualReviewEnvironment environment)
    {
        _environment = environment;
    }

    public override string ToString()
    {
        var escaped =
            new
            {
                Url = HttpUtility.HtmlEncode(_environment.GitLabUrl),
                ProjectId = HttpUtility.HtmlEncode(_environment.GitLabProjectId),
                ProjectPath = HttpUtility.HtmlEncode(_environment.GitLabProjectPath),
                MergeRequestId = HttpUtility.HtmlEncode(_environment.GitLabMergeRequestId)
            };

        return $@"
<script
    data-project-id=""{escaped.ProjectId}""
    data-merge-request-id=""{escaped.MergeRequestId}""
    data-mr-url=""{escaped.Url}""
    data-project-path=""{escaped.ProjectPath}""
    id=""review-app-toolbar-script""
    src=""https://gitlab.com/assets/webpack/visual_review_toolbar.js"">
</script>
";
    }

    public static implicit operator string(VisualReviewsScript script)
    {
        return script.ToString();
    }
}