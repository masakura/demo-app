using System.Net.Mime;
using Microsoft.AspNetCore.Http;

namespace GitLab.VisualReviews;

// ReSharper disable once ClassNeverInstantiated.Global
internal sealed class VisualReviewsMiddleware
{
    private readonly RequestDelegate _next;

    public VisualReviewsMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    // ReSharper disable once UnusedMember.Global
    public async Task InvokeAsync(HttpContext context)
    {
        var original = context.Response.Body;
        using var buffer = new MemoryStream();
        context.Response.Body = buffer;

        await _next(context);
        if (!IsTextHtml(context)) return;

        context.Response.Body = original;
        buffer.Position = 0;

        var contentType = new ContentType(context.Response.ContentType);
        if (contentType.MediaType != MediaTypeNames.Text.Html)
        {
            await buffer.CopyToAsync(original);
            return;
        }

        var html = await Html.ReadAsync(buffer, contentType);
        var environment = VisualReviewEnvironment.Current;
        await context.Response.WriteAsync(html.InsertScriptToEnd(environment.Script()));
    }

    private static bool IsTextHtml(HttpContext context)
    {
        if (context.Response == null) return false;
        if (context.Response.ContentType == null) return false;
        return context.Response.ContentType.StartsWith("text/html");
    }
}