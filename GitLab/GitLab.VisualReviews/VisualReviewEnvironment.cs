namespace GitLab.VisualReviews;

internal sealed record VisualReviewEnvironment
{
    private static readonly Lazy<VisualReviewEnvironment?> Lazy = new(Create);

    private VisualReviewEnvironment(string gitLabUrl,
        string gitLabProjectId,
        string gitLabProjectPath,
        string gitLabMergeRequestId)
    {
        GitLabMergeRequestId = gitLabMergeRequestId;
        GitLabUrl = gitLabUrl;
        GitLabProjectId = gitLabProjectId;
        GitLabProjectPath = gitLabProjectPath;
    }

    public static VisualReviewEnvironment Current => Lazy.Value!;
    public static bool RequireVisualReviews => Lazy.Value != null;
    public string GitLabMergeRequestId { get; }
    public string GitLabUrl { get; }
    public string GitLabProjectId { get; }
    public string GitLabProjectPath { get; }

    private static VisualReviewEnvironment? Create()
    {
        var mergeRequestId = Environment.GetEnvironmentVariable("CI_MERGE_REQUEST_IID");
        if (string.IsNullOrEmpty(mergeRequestId)) return null;

        return new VisualReviewEnvironment(
            Environment.GetEnvironmentVariable("CI_SERVER_URL")!,
            Environment.GetEnvironmentVariable("CI_PROJECT_ID")!,
            Environment.GetEnvironmentVariable("CI_PROJECT_PATH")!,
            mergeRequestId
        );
    }

    public VisualReviewsScript Script()
    {
        return new VisualReviewsScript(this);
    }
}