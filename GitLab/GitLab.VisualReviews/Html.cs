using System.Net.Mime;
using System.Text;

namespace GitLab.VisualReviews;

internal sealed class Html
{
    private readonly string _text;

    private Html(string text)
    {
        _text = text;
    }

    public Html InsertScriptToEnd(string script)
    {
        return new Html(_text.Replace("</body>", $"{script}</body>"));
    }

    public static async Task<Html> ReadAsync(Stream stream, ContentType contentType)
    {
        stream.Position = 0;
        using var reader = new StreamReader(stream, GetEncoding(contentType));
        return new Html(await reader.ReadToEndAsync());
    }

    public override string ToString()
    {
        return _text;
    }

    public static implicit operator string(Html html)
    {
        return html.ToString();
    }

    private static Encoding GetEncoding(ContentType contentType)
    {
        if (!string.IsNullOrWhiteSpace(contentType.CharSet)) return Encoding.Default;

        return Encoding.GetEncoding(contentType.CharSet!);
    }
}