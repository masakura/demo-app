using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Unleash;

namespace GitLab.FeatureFlags;

public static class FeatureFlagsServiceCollectionExtensions
{
    // ReSharper disable once UnusedMethodReturnValue.Global
    public static IServiceCollection AddFeatureFlags(this IServiceCollection services)
    {
        return services.AddSingleton(CreateUnleash);
    }

    private static IUnleash CreateUnleash(IServiceProvider services)
    {
        var environment = services.GetRequiredService<IHostEnvironment>();

        var settings = new UnleashSettings
        {
            AppName = environment.EnvironmentName.ToLower(),
            UnleashApi = new Uri("https://gitlab.com/api/v4/feature_flags/unleash/32006334"),
            InstanceTag = "gzbtXyfh2yqVcibCJbyW",
            FetchTogglesInterval = TimeSpan.FromSeconds(10)
        };
        return new DefaultUnleash(settings);
    }
}