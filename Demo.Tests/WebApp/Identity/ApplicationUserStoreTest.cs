using System;
using System.Linq;
using System.Threading.Tasks;
using Demo.WebApp.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using PowerAssert;
using Xunit;

namespace Demo.WebApp.Identity;

public sealed class ApplicationUserStoreTest : IDisposable
{
    private readonly WebApplication _host;
    private readonly IServiceScope _scope;
    private readonly ApplicationUser _user;
    private readonly UserManager<ApplicationUser> _userManager;

    public ApplicationUserStoreTest()
    {
        var builder = WebApplication.CreateBuilder();
        builder.AddDatabase();
        builder.Services
            .AddLogging()
            .AddApplicationIdentity()
            .BuildServiceProvider();

        _host = builder.Build();
        _scope = _host.Services.CreateScope();

        _userManager = _scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

        _user = ApplicationUser.New(Guid.NewGuid().ToString());
    }

    public void Dispose()
    {
        _userManager.DeleteAsync(_user).Wait();

        _scope.Dispose();
        ((IDisposable) _host).Dispose();
    }

    [Fact]
    public async Task ユーザーの追加()
    {
        var result = await _userManager.CreateAsync(_user);

        PAssert.IsTrue(() => result.Succeeded);
    }

    [Fact]
    public async Task ユーザーの削除()
    {
        await _userManager.CreateAsync(_user);

        var result = await _userManager.DeleteAsync(_user);

        PAssert.IsTrue(() => result.Succeeded);
    }

    [Fact]
    public async Task サインイン()
    {
        await _userManager.CreateAsync(_user);
        await _userManager.AddPasswordAsync(_user, "P@ssw0rd");

        var actual = await _userManager.CheckPasswordAsync(_user, "P@ssw0rd");

        PAssert.IsTrue(() => actual);
    }

    [Fact]
    public async Task ユーザー一覧()
    {
        await _userManager.CreateAsync(_user);

        var result = _userManager.Users.First(user => user.Id == _user.Id);

        var actual = new {result.Id, result.UserName};
        var expected = new {_user.Id, _user.UserName};

        PAssert.IsTrue(() => actual.Equals(expected));
    }

    [Fact]
    public async Task FindByIdAsync()
    {
        await _userManager.CreateAsync(_user);

        var result = await _userManager.FindByIdAsync(_user.Id.ToString());

        var actual = new {result.Id, result.UserName};
        var expected = new {_user.Id, _user.UserName};

        PAssert.IsTrue(() => actual.Equals(expected));
    }
}